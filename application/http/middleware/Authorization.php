<?php

namespace app\http\middleware;

use Casbin;
use think\Facade\Session;

class Authorization
{
    public function handle($request, \Closure $next)
    {
        //获取session里的用户名
        $user = Session::get("user.username");

        //获取url和action
        $url = $request->url();
        $action =  $request->method();
        //认证判断
        if(!$user){
            return response()->data("Unauthenticated!")->code(401);
        }
        //授权判断
        if(Casbin::enforce($user,$url,$action)){
            return response()->data("Unauthorized!")->code(403);
        }
        return $next($request);

    }
}
