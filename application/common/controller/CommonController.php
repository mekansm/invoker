<?php

namespace app\common\controller;

use think\App;
use think\Controller;
use Config;
use think\facade\Session;
use think\Request;

class CommonController extends Controller
{
    public function __construct(App $app = null)
    {
        parent::__construct($app);
        //模板数据
        $this->assign('cssPath', Config::get('config.cssPath'));
        $this->assign('jsPath', Config::get('config.jsPath'));
        $this->assign('iPath', Config::get('config.iPath'));
        $this->assign('imgPath', Config::get('config.imgPath'));
        //从session中获取昵称
        $this->assign('nickname', Session::get('user.nickname'));


    }
}
