<?php

namespace app\admin\validate;

use think\Validate;

class UserLogin extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
        "username"=> "require",
        "password"=> "require",
    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */	
    protected $message = [
        'password.require'	=>	'用户名是必填项!',
        'username.require'	=>	'密码是必填项!',
    ];
}
