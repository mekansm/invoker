<?php

namespace app\admin\validate;

use think\Validate;

class UserRegister extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
        "username"=> "require|min:3",
        "nickname"=> "require",
        "password"=> "require|min:6",
    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */	
    protected $message = [
        'username.require'	=>	'用户名是必填项!',
        'nickname.require'	=>	'昵称是必填项!',
        'password.require'	=>	'密码是必填项!',
        'username.min'	=>	'用户名不能少于3个字符!',
        'password.min'	=>	'密码不能少于6个字符!',
    ];
}
