<?php

namespace app\admin\controller;

use think\App;
use think\Controller;
use think\Request;
use Config;
use app\admin\model\User;
use think\facade\Session;

class LoginController extends Controller
{
    //开启批量验证
    protected $batchValidate = true;


    public function __construct(App $app = null)
    {
        parent::__construct($app);
        //模板数据
        $this->assign('cssPath', Config::get('config.cssPath'));
        $this->assign('jsPath', Config::get('config.jsPath'));
        $this->assign('iPath', Config::get('config.iPath'));
        $this->assign('imgPath', Config::get('config.imgPath'));
    }

    /**
     * 登录显示页面
     *
     */
    public function login()
    {
        //自定义数据
        $this->assign('title', '登录页面');
        return $this->fetch();
    }

    /*
     * 执行登录
     * */
    public function todoLogin(Request $request)
    {
        $input_username = $request->post("username");
        $input_password = $request->post("password");

        //输入数据验证结果
        $vaildateResult = $this->validate([
                "username" => $input_username,
                "password" => $input_password
            ]
            , 'app\admin\validate\UserLogin');
        if (is_array($vaildateResult)) {
            return json($vaildateResult);
        }

        $db_user_username = User::where('username', $input_username)->find();
        $db_user_password = $db_user_username["password"];
        $db_user_encrypt = $db_user_username["encrypt"];

        if (!$db_user_username || $db_user_password != get_password($input_password, $db_user_encrypt)) {
            return json("登录失败，用户不存在或密码不正确！")->code(200);
        }

        session("user", $db_user_username);
        return redirect("/admin/index");
    }

    /*
     * 注册展示页面
     * */
    public function register()
    {
        return $this->fetch();
    }

    public function todoRegister(Request $request)
    {
        //数据
        $input_username = $request->post("username");
        $input_nickname = $request->post("nickname");
        $input_password = $request->post("password");

        //输入数据验证结果
        $vaildateResult = $this->validate([
                "username" => $input_username,
                "nickname" => $input_nickname,
                "password" => $input_password
            ]
            , 'app\admin\validate\UserRegister');
        if (is_array($vaildateResult)) {
            return json($vaildateResult);
        }

        //判断用户名是否已经存在，重复
        $userQuery = User::where('username', $input_username)->count();
        if ($userQuery > 0) {
            $data = ["message" => "用户名已存在，请重新填写！"];
            return json($data)->code(200);
        }

        //加密
        $md5_arr = get_password($input_password);

        //添加
        $userModel = new User;
        $userModel->username = $input_username;
        $userModel->nickname = $input_nickname;
        $userModel->password = $md5_arr["password"];
        $userModel->encrypt = $md5_arr["encrypt"];
        $result = $userModel->save();

        //响应
        if (!$result) {
            $data = ["message" => "注册失败！"];
            return json($data)->code(200);
        }
        return redirect("/login");
    }

    /*
     * 登出
     * */
    public function loginOut(Request $request){
        Session::clear();
        return redirect("/login");
    }

}
