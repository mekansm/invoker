<?php
namespace app\admin\controller;

use app\common\controller\CommonController;
use think\App;
use think\Controller;
use Config;
use think\facade\Session;
use think\facade\Env;

class IndexController extends  CommonController
{
    public function __construct(App $app = null)
    {
        parent::__construct($app);

    }

    public function index()
    {
        //自定义数据
        $this->assign('title', 'invoker管理系统');
        //使用模板布局
        $this->view->engine->layout('layout/layout01');
        return $this->fetch();
    }

    public function test(){
        //abort(404, '页面异常');
        return Env::get('app_path') .'404.html';
        //return APP_PATH;
    }
}
