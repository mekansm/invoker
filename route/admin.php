<?php
//登录
Route::get('login', 'admin/LoginController/login');
Route::post('login', 'admin/LoginController/todoLogin');
//注册
Route::get('register', 'admin/LoginController/register');
Route::post('register', 'admin/LoginController/todoRegister');
//登出
Route::get('loginOut','admin/LoginController/loginOut');

//内部逻辑
Route::Group("admin",function (){
    Route::get('/index', 'admin/IndexController/index');
    //用户
    Route::get('/user/index', 'admin/UserController/index');
    Route::post('/user/save', 'admin/UserController/save');

})->middleware(\app\http\middleware\Authorization::class);
